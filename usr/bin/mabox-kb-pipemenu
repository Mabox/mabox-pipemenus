#!/bin/bash
#    mabox-kb-pipemenu - an Openbox pipemenu for displaying keybinds
#    Copyright (C) 2015 damo    <damo@bunsenlabs.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# NB The keybinds in the menu are clickable, except for the Openbox
# commands, which just exit the menu

# mb-kb is used to parse rc.xml and send the output to a textfile

MB_COMMON_LIBDIR="/usr/lib/mabox/mabox-common"
USR_CFG_DIR="$HOME/.config/openbox"
KBSCRIPT="mb-kb"
RCPATH="$USR_CFG_DIR/rc.xml"
KBTEXT="$USR_CFG_DIR/kbinds.txt"

case $LANG in
    pl*)
    BKP_EDIT="Backup oraz edycja rc.xml"
    DISPLAY_IN_WINDOW="Pokaż w nowym oknie"
    DISPLAY_IN_MENU="Pokaż w menu"
    WINDOW_COMMANDS="Zarządzanie oknami"
    RUN_COMMANDS="Klawisze uruchamiania"
    ;;
    es*)
    BKP_EDIT="Respaldar y Editar archivo rc.xml"
    DISPLAY_IN_WINDOW="Mostrar Atajos de teclado en una nueva ventana"
    DISPLAY_IN_MENU="Mostrar Atajos de teclado en el menú"
    WINDOW_COMMANDS="Comandos para ventanas en Openbox"
    RUN_COMMANDS="'Ejecutar' commandos"
    ;;
    *)
    BKP_EDIT="Backup and Edit rc.xml"
    DISPLAY_IN_WINDOW="Display Keybinds in new window"
    DISPLAY_IN_MENU="Display Keybinds in menu"
    WINDOW_COMMANDS="Openbox window commands"
    RUN_COMMANDS="'Run' commands"
    ;;
esac

if ! . "$MB_COMMON_LIBDIR/mabox-include.cfg" 2> /dev/null; then
    echo $"Error: Failed to locate mabox-include.cfg in $MB_COMMON_LIBDIR" >&2
    exit 1
fi

# test if mb-kb exists in path
if type "$KBSCRIPT" &> /dev/null;then
    # run script to write kbinds.txt, suppress terminal output
    "$KBSCRIPT" > /dev/null
else
    menuStart
    menuSeparator "$KBSCRIPT is not available. Try installing mabox-utilities."
    menuEnd
    exit 1
fi

if [[ $1 = '--rcedit' ]];then # "Backup & Edit rc.xml" is chosen in menu
    # backup rc.xml first
    NOW=$(date +"%Y%m%d-%H%M")
    RCBKP="$RCPATH.$NOW"
    cp "$RCPATH" "$RCBKP"
    xdg-open "$RCPATH"        # open rc.xml in default editor

fi

# pipemenu
menuStart
    menuItem "$BKP_EDIT" "mabox-kb-pipemenu --rcedit"
    #menuItem "$GUI_EDIT" "obkey"
    menuSeparator
    menuItem "$DISPLAY_IN_WINDOW" "mb-kb --gui"
    menuSubmenu "Display keybinds" "$DISPLAY_IN_MENU"
        menuSeparator "$RUN_COMMANDS"
        while read -r a b c;do      # write out 'Run' commands (clickable)
            if [[ "$a" = "x" ]];then
                curItem=$(printf "%-23s %s" "$b" "$c")
                menuItem "$curItem" "$c"
            fi
        done < "$KBTEXT"
        menuSeparator
        menuSeparator "$WINDOW_COMMANDS"
        while read -r a b c;do      # write out Openbox commands (not clickable)
            if [[ "$a" = "o" ]];then
                curItem=$(printf "%-20s %s" "$b" "$c")
                menuItem "$curItem" "echo >/dev/null 2>&1"
            fi
        done < "$KBTEXT"
        
    menuSubmenuEnd
menuEnd

exit 0
